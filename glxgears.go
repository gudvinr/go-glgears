package main

import (
	"fmt"
	"math"
	"runtime"
	"time"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
)

var gear1 uint32
var gear2 uint32
var gear3 uint32
var angle float32

var viewRotx = float32(20.0)
var viewRoty = float32(30.0)
var viewRotz = float32(0.0)

var animate = true

var frames uint32

var tRot0 = float64(-1.0)
var tRate0 = float64(-1.0)

//
// float32 version of math funcs
//

func cos(angle float32) float32 {
	return float32(math.Cos(float64(angle)))
}

func sin(angle float32) float32 {
	return float32(math.Sin(float64(angle)))
}

func sqrt(x float32) float32 {
	return float32(math.Sqrt(float64(x)))
}

func init() {
	runtime.LockOSThread()
}

func main() {
	var winWidth int32 = 300
	var winHeight int32 = 300

	if err := glfw.Init(); err != nil {
		panic(err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.False)
	window, err := glfw.CreateWindow(300, 300, "GLGears", nil, nil)
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	if err := gl.Init(); err != nil {
		panic(err)
	}

	fmt.Println("Running OpenGL", glfw.GetVersionString())

	initGL()

	reshape(winWidth, winHeight)

	eventLoop(window)

	gl.DeleteLists(gear1, 1)
	gl.DeleteLists(gear2, 1)
	gl.DeleteLists(gear3, 1)
}

func keyCallback(win *glfw.Window, key glfw.Key, scancode int, action glfw.Action, mods glfw.ModifierKey) {
	if action == glfw.Release {
		return
	}

	switch key {
	case glfw.KeyLeft:
		viewRoty += 5.0
	case glfw.KeyRight:
		viewRoty -= 5.0
	case glfw.KeyUp:
		viewRotx += 5.0
	case glfw.KeyDown:
		viewRotx -= 5.0
	case glfw.KeyEscape:
		if action == glfw.Repeat {
			break
		}
		win.SetShouldClose(true)
	case glfw.KeyA:
		if action == glfw.Repeat {
			break
		}
		animate = !animate
	}
}

func eventLoop(win *glfw.Window) {
	win.SetKeyCallback(keyCallback)

	for !win.ShouldClose() {
		drawFrame(win)
		glfw.PollEvents()
	}
}

func draw() {
	gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

	gl.PushMatrix()
	gl.Rotatef(viewRotx, 1.0, 0.0, 0.0)
	gl.Rotatef(viewRoty, 0.0, 1.0, 0.0)
	gl.Rotatef(viewRotz, 0.0, 0.0, 1.0)

	gl.PushMatrix()
	gl.Translatef(-3.0, -2.0, 0.0)
	gl.Rotatef(angle, 0.0, 0.0, 1.0)
	gl.CallList(gear1)
	gl.PopMatrix()

	gl.PushMatrix()
	gl.Translatef(3.1, -2.0, 0.0)
	gl.Rotatef(-2.0*angle-9.0, 0.0, 0.0, 1.0)
	gl.CallList(gear2)
	gl.PopMatrix()

	gl.PushMatrix()
	gl.Translatef(-3.1, 4.2, 0.0)
	gl.Rotatef(-2.0*angle-25.0, 0.0, 0.0, 1.0)
	gl.CallList(gear3)
	gl.PopMatrix()

	gl.PopMatrix()
}

/** Draw single frame, do SwapBuffers, compute FPS */
func drawFrame(win *glfw.Window) {
	var dt float32
	t := float64(time.Now().UnixNano()) / 1000000000.0

	if tRot0 < 0.0 {
		tRot0 = t
	}

	dt = float32(t - tRot0)
	tRot0 = t

	if animate {
		/* advance rotation for next frame */
		angle += 70.0 * dt /* 70 degrees per second */
		if angle > 3600.0 {
			angle -= 3600.0
		}
	}

	draw()
	win.SwapBuffers()

	frames++

	if tRate0 < 0.0 {
		tRate0 = t
	}

	if t-tRate0 >= 5.0 {
		seconds := float32(t - tRate0)
		fps := float32(frames) / seconds
		fmt.Printf("%d frames in %3.1f seconds = %6.3f FPS\n", frames, seconds, fps)
		tRate0 = t
		frames = 0
	}
}

func gear(innerRadius, outerRadius, width float32, teeth uint32, toothDepth float32) {
	fteeth := float32(teeth)

	var angle, u, v, len float32

	r0 := float32(innerRadius)
	r1 := float32(outerRadius - toothDepth/2.0)
	r2 := float32(outerRadius + toothDepth/2.0)

	da := float32(2.0 * math.Pi / fteeth / 4.0)

	gl.ShadeModel(gl.FLAT)

	gl.Normal3f(0.0, 0.0, 1.0)

	/* draw front face */
	gl.Begin(gl.QUAD_STRIP)
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth
		gl.Vertex3f(r0*cos(angle), r0*sin(angle), width*0.5)
		gl.Vertex3f(r1*cos(angle), r1*sin(angle), width*0.5)
		if i < fteeth {
			gl.Vertex3f(r0*cos(angle), r0*sin(angle), width*0.5)
			gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
				width*0.5)
		}
	}
	gl.End()

	/* draw front sides of teeth */
	gl.Begin(gl.QUADS)
	da = 2.0 * math.Pi / fteeth / 4.0
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth

		gl.Vertex3f(r1*cos(angle), r1*sin(angle), width*0.5)
		gl.Vertex3f(r2*cos(angle+da), r2*sin(angle+da), width*0.5)
		gl.Vertex3f(r2*cos(angle+2*da), r2*sin(angle+2*da),
			width*0.5)
		gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
			width*0.5)
	}
	gl.End()

	gl.Normal3f(0.0, 0.0, -1.0)

	/* draw back face */
	gl.Begin(gl.QUAD_STRIP)
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth
		gl.Vertex3f(r1*cos(angle), r1*sin(angle), -width*0.5)
		gl.Vertex3f(r0*cos(angle), r0*sin(angle), -width*0.5)
		if i < fteeth {
			gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
				-width*0.5)
			gl.Vertex3f(r0*cos(angle), r0*sin(angle), -width*0.5)
		}
	}
	gl.End()

	/* draw back sides of teeth */
	gl.Begin(gl.QUADS)
	da = 2.0 * math.Pi / fteeth / 4.0
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth

		gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
			-width*0.5)
		gl.Vertex3f(r2*cos(angle+2*da), r2*sin(angle+2*da),
			-width*0.5)
		gl.Vertex3f(r2*cos(angle+da), r2*sin(angle+da), -width*0.5)
		gl.Vertex3f(r1*cos(angle), r1*sin(angle), -width*0.5)
	}
	gl.End()

	/* draw outward faces of teeth */
	gl.Begin(gl.QUAD_STRIP)
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth

		gl.Vertex3f(r1*cos(angle), r1*sin(angle), width*0.5)
		gl.Vertex3f(r1*cos(angle), r1*sin(angle), -width*0.5)
		u = r2*cos(angle+da) - r1*cos(angle)
		v = r2*sin(angle+da) - r1*sin(angle)
		len = sqrt(u*u + v*v)
		u /= len
		v /= len
		gl.Normal3f(v, -u, 0.0)
		gl.Vertex3f(r2*cos(angle+da), r2*sin(angle+da), width*0.5)
		gl.Vertex3f(r2*cos(angle+da), r2*sin(angle+da), -width*0.5)
		gl.Normal3f(cos(angle), sin(angle), 0.0)
		gl.Vertex3f(r2*cos(angle+2*da), r2*sin(angle+2*da),
			width*0.5)
		gl.Vertex3f(r2*cos(angle+2*da), r2*sin(angle+2*da),
			-width*0.5)
		u = r1*cos(angle+3*da) - r2*cos(angle+2*da)
		v = r1*sin(angle+3*da) - r2*sin(angle+2*da)
		gl.Normal3f(v, -u, 0.0)
		gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
			width*0.5)
		gl.Vertex3f(r1*cos(angle+3*da), r1*sin(angle+3*da),
			-width*0.5)
		gl.Normal3f(cos(angle), sin(angle), 0.0)
	}

	gl.Vertex3f(r1*cos(0), r1*sin(0), width*0.5)
	gl.Vertex3f(r1*cos(0), r1*sin(0), -width*0.5)

	gl.End()

	gl.ShadeModel(gl.SMOOTH)

	/* draw inside radius cylinder */
	gl.Begin(gl.QUAD_STRIP)
	for i := float32(0); i <= fteeth; i++ {
		angle = i * 2.0 * math.Pi / fteeth
		gl.Normal3f(-cos(angle), -sin(angle), 0.0)
		gl.Vertex3f(r0*cos(angle), r0*sin(angle), -width*0.5)
		gl.Vertex3f(r0*cos(angle), r0*sin(angle), width*0.5)
	}
	gl.End()
}

func initGL() {
	var pos = [4]float32{5.0, 5.0, 10.0, 0.0}
	var red = [4]float32{0.8, 0.1, 0.0, 1.0}
	var green = [4]float32{0.0, 0.8, 0.2, 1.0}
	var blue = [4]float32{0.2, 0.2, 1.0, 1.0}

	gl.Lightfv(gl.LIGHT0, gl.POSITION, &pos[0])
	gl.Enable(gl.CULL_FACE)
	gl.Enable(gl.LIGHTING)
	gl.Enable(gl.LIGHT0)
	gl.Enable(gl.DEPTH_TEST)

	/* make the gears */
	gear1 = gl.GenLists(1)
	gl.NewList(gear1, gl.COMPILE)
	gl.Materialfv(gl.FRONT, gl.AMBIENT_AND_DIFFUSE, &red[0])
	gear(1.0, 4.0, 1.0, 20, 0.7)
	gl.EndList()

	gear2 = gl.GenLists(1)
	gl.NewList(gear2, gl.COMPILE)
	gl.Materialfv(gl.FRONT, gl.AMBIENT_AND_DIFFUSE, &green[0])
	gear(0.5, 2.0, 2.0, 10, 0.7)
	gl.EndList()

	gear3 = gl.GenLists(1)
	gl.NewList(gear3, gl.COMPILE)
	gl.Materialfv(gl.FRONT, gl.AMBIENT_AND_DIFFUSE, &blue[0])
	gear(1.3, 2.0, 0.5, 10, 0.7)
	gl.EndList()

	gl.Enable(gl.NORMALIZE)
}

func reshape(width, height int32) {
	gl.Viewport(0, 0, width, height)

	h := float64(height) / float64(width)

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Frustum(-1.0, 1.0, -h, h, 5.0, 60.0)

	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()
	gl.Translatef(0.0, 0.0, -40.0)
}
